# language: pt

@all @req001

Funcionalidade: REQ001 - Calcular Desconto do Sistema

  Esquema do Cenario: Acesso à Desconto Calculado via clique de botão de calcular desconto página de Calcular desconto

    Dado   que estou na página de listagem de produtos do sistema

    E  clico no botão de desconto

    E  clico no botão de calcular desconto

    Entao  verifico que estou na página de desconto calculado
      | titulo | <titulo> |

    Exemplos:
      | titulo                |
      | Descontos Alcançados  |
      | Descontos Alcançados  |
      | Descontos Alcançados  |




# language: pt

@all @req001

Funcionalidade: REQ001 - Calcular Desconto do Sistema

  Esquema do Cenario: Acesso à Cálculo de Desconto via clique de botão calcular desconto na listagem de produtos

    Dado   que estou na página de listagem de produtos do sistema

    Quando  clico no botão de desconto

    Entao  verifico que estou na página de calcular desconto
      | titulo | <titulo> |
      | descricao  | <descricao>  |

    Exemplos:
      | titulo                              | descricao                                                                                   |
      | Cálculo do Desconto para o Produto  | Verifique os dados abaixo a fim de identificar se o produto foi selecionado corretamente:   |
      | Cálculo do Desconto para o Produto  | Verifique os dados abaixo a fim de identificar se o produto foi selecionado corretamente2:  |
      | Cálculo do Desconto para o Produto  | Verifique os dados abaixo a fim de identificar se o produto foi selecionado corretamente3:  |




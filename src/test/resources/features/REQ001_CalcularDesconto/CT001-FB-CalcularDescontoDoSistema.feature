# language: pt

@all @req001

Funcionalidade: REQ001 - Calcular desconto do Sistema

  Esquema do Cenario: Acesso à Cálculo de Desconto via menu principal

    Dado   que estou na página inicial do sistema

    Quando clico no menu de acesso à funcionalidade atual
      | menu | <menu>  |

    Entao  verifico que estou na página de listar produtos
      | titulo | <titulo> |
      | descricao  | <descricao>  |

    Exemplos:
    | menu                | titulo                | descricao                                                       |
    | Calcular Desconto   | Listagem de Produtos  | Selecione um produto dentre os disponíveis na listagem abaixo:  |
    | Calcular Desconto   | Listagem de Produtos  | Selecione um produto dentre os disponíveis na listagem abaixo1: |
    | Calcular Desconto   | Listagem de Produtos  | Selecione um produto dentre os disponíveis na listagem abaixo2: |

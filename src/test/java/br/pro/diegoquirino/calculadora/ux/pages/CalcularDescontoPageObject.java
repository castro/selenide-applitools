package br.pro.diegoquirino.calculadora.ux.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class CalcularDescontoPageObject {

    public static void clickBotaoDesconto() {
        $(By.xpath("/html/body/div[1]/div[2]/table/tbody/tr[1]/td[4]/a[1]")).click();
    }

    public static void clickBotaoCalcularDesconto() {
        $(By.id("calculardesconto.button.calcular")).shouldHave(text("Calcular Desconto!")).click();
    }

    public static String getListarProdutosPageTitle() {
        return $(By.xpath("/html/body/div[1]/div[2]/h1")).getText();
    }

    public static String getListarProdutosPageDescription() {
        return $(By.xpath("/html/body/div[1]/div[2]/p")).getText();
    }

    public static String getCalcularDescontoPageTitle() {
        return $(By.xpath("/html/body/div[1]/div[2]/h1")).getText();
    }

    public static String getDescontoCalculadoPageTitle() {
        return $(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]")).getText();
    }

    public static String getCalcularDescontoPageDescription() {
        return $(By.xpath("/html/body/div[1]/div[2]/p[1]")).getText();
    }

}

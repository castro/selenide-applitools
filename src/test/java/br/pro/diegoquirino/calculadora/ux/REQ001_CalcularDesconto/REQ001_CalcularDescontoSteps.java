package br.pro.diegoquirino.calculadora.ux.REQ001_CalcularDesconto;

import br.pro.diegoquirino.calculadora.ux.EyesSingleton;
import br.pro.diegoquirino.calculadora.ux.pages.CalcularDescontoPageObject;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import net.thucydides.core.webdriver.WebDriverFacade;

import static org.junit.Assert.assertTrue;

public class REQ001_CalcularDescontoSteps extends ScenarioSteps {

    CalcularDescontoPageObject page;

    @Step
    public void verifico_estou_listar_produtos_do_sistema(String titulo, String descricao) {
        assertTrue(this.page.getListarProdutosPageTitle().equalsIgnoreCase(titulo));
        assertTrue(this.page.getListarProdutosPageDescription().equalsIgnoreCase(descricao));
    }

    @Step
    public void verifico_se_estou_em_calcular_desconto_de_produto(String titulo, String descricao) {
        assertTrue(this.page.getCalcularDescontoPageTitle().equalsIgnoreCase(titulo));
        assertTrue(this.page.getCalcularDescontoPageDescription().equalsIgnoreCase(descricao));
    }

    @Step
    public void verifico_se_estou_em_desconto_caculado(String titulo) {
        assertTrue(this.page.getDescontoCalculadoPageTitle().equalsIgnoreCase(titulo));
    }

    @Step
    public void clickBotaoDesconto() {
        this.page.clickBotaoDesconto();
        EyesSingleton.simpleCheck((WebDriverFacade) this.getDriver(),
                "Acesso à página de CALCULAR DESCONTO de produto",
                "pagina.calcular-desconto-de-produtos");
    }

    @Step
    public void clickBotaoCalcularDesconto() {
        this.page.clickBotaoCalcularDesconto();
        EyesSingleton.simpleCheck((WebDriverFacade) this.getDriver(),
                "Acesso à página de DESCONTO CALCULADO",
                "pagina.desconto-calculado");
    }
}

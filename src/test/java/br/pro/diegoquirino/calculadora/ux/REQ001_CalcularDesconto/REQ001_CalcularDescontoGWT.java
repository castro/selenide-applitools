package br.pro.diegoquirino.calculadora.ux.REQ001_CalcularDesconto;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.Map;

public class REQ001_CalcularDescontoGWT {

    @Steps
    REQ001_CalcularDescontoSteps steps;

    @Entao("^verifico que estou na página de listar produtos")
    public void verifico_que_estou_na_página_de_listar_produtos(DataTable tabela) throws Exception {
        Map<String, String> dados = tabela.asMap(String.class, String.class);

        steps.verifico_estou_listar_produtos_do_sistema(
                dados.get("titulo"),
                dados.get("descricao")
        );
    }

    @Entao("^verifico que estou na página de calcular desconto")
    public void verifico_que_estou_na_página_de_calcular_desconto(DataTable tabela) throws Exception {
        Map<String, String> dados = tabela.asMap(String.class, String.class);

        steps.verifico_se_estou_em_calcular_desconto_de_produto(
                dados.get("titulo"),
                dados.get("descricao")
        );
    }

    @Entao("^verifico que estou na página de desconto calculado")
    public void verifico_que_estou_na_página_de_desconto_calculado(DataTable tabela) throws Exception {
        Map<String, String> dados = tabela.asMap(String.class, String.class);

        steps.verifico_se_estou_em_desconto_caculado(
                dados.get("titulo")
        );
    }

    @Quando("^clico no botão de desconto$")
    public void clico_no_botão_de_desconto() throws Exception {
        steps.clickBotaoDesconto();
    }

    @Quando("^clico no botão de calcular desconto$")
    public void clico_no_botão_de_calcular_desconto() throws Exception {
        steps.clickBotaoCalcularDesconto();
    }
}
